/* Theme Name: Worthy - Free Powerful Theme by HtmlCoder
 * Author:HtmlCoder
 * Author URI:http://www.htmlcoder.me
 * Version:1.0.0
 * Created:November 2014
 * License: Creative Commons Attribution 3.0 License (https://creativecommons.org/licenses/by/3.0/)
 * File Description: Place here your custom scripts
 */



var header = $('.cta-header');
var banner = $('.banner');
header.hide();
// var whenToHide = (header.offset().top + header.height()) / 2;
var whenToHide = banner.height();
var triggerTresHide = banner.offset().top + banner.height()+ banner.height();
var closedpressed = 0;
var oktohide = 0;

$('.cta-close').on("click",function(){
	closedpressed = 1;
	$('.cta-header').children().slideUp();
	$('.cta-header').slideUp();
	
	
});
$(window).scroll(function () {
	console.log(closedpressed);
    var offset = header.offset();
    if(offset.top >= triggerTresHide){
    	oktohide = 1;
    }
    if(closedpressed == 0){
	    	if (offset.top <= whenToHide && oktohide ==1) {
	        header.slideDown();
	        	header.children().slideDown();

	    } else {
	       	header.children().slideUp();
	        header.slideUp();
	    }
    }
    
});